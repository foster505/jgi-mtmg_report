#!/usr/bin/env python

import sys
import os
import re
import collections
import argparse
import json
import glob
import time
import subprocess
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
#sys.path.insert(0,"/global/projectb/sandbox/gaag/bfoster/meta.dev/jgi-mt/lib")

'''
analysis_tool object tool object with directory, date tool name directory.
     child populates version, command line, reference, readme, stats

to be done: rqcfilter
in progress: megahit

'''
def main ():
    choices=['spades', 'metaspades','bbmap','bfc','dedupe','tadpole','bbnorm', 'megahit','bbcms']
    parser = argparse.ArgumentParser(description='Setup script that takes input files and generates metadata for pipelines/JAT submission.')
    parser.add_argument('-t', "--tool", required=True, dest='tool', action='store',choices=choices, help="product type to run.\n")
    parser.add_argument('-d', "--directory", required=True, help="tool directory\n")
    parser.add_argument('-s', "--statsfile", required=False, default="",help="stats file to parse\n")
    parser.add_argument('-l', "--template", required=False, default="metagenome",help="template\n")
#    parser.add_argument("-o", "--outputfile", required=False, help="output file json\n")
    args = parser.parse_args()

    if not  os.path.exists(args.directory):
        sys.exit(args.directory  + "Does not exist\n")

    if args.statsfile and  not os.path.exists(args.statsfile):
        sys.exit(args.directory  + "Does not exist\n")
        
    class_name = eval('Analysis_' + args.tool)

    tool = class_name(directory=args.directory,tool_name=args.tool,statsfile=args.statsfile,template=args.template)
    print(str(tool))

    return


class Analysis_tool(object):
    '''analysis_tool master class is an object that holds information about an analysis task
       packaged in a way for ease of reporting
       should capture:
           directory, date, tool_name
               child populates version, command line, reference, readme, stats
    '''
    
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        '''init'''
        self.directory=directory
        self.tool_name=tool_name
        self.statsfile = statsfile
        self.template = template
        if os.path.exists(self.directory):
            globs = glob.glob(os.path.join(self.directory,'*'))
            #get newest file in directory
            globs.sort(key=os.path.getmtime)
            self.date= os.path.getmtime(globs[-1])
        else:
            self.date= time.time()
        self.readme_prefix= ""
        self.version= ""
        self.command_line= ""
        self.reference = ""
        self.readme = ""

    def get_readme(self):
        readme = ""
        if self.readme_prefix:
            readme += self.readme_prefix
        if self.tool_name and self.version:
            readme += " with " + self.tool_name + " " + self.version + "."
        if self.reference:
            readme += "(__REF__) " 
        if self.command_line:
            readme += "This was run using the following command line options: "
            readme += self.command_line + "."
        return readme

    def dump(self):
        info = dict()
        info['readme'] = self.readme
        info['reference'] = self.reference
        info['stats'] = self.stats
        info['metadata_json']= self.metadata
        return info

    def __str__(self):
        return json.dumps(self.dump(),indent=3)
                          


class Analysis_bfc(Analysis_tool):
    '''given an analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        self.readme_prefix = "Reads were corrected using bfc and reads with no mate pair were removed"
        self.reference = "BFC: correcting Illumina sequencing errors. - Bioinformatics. 2015 Sep1;31(17):2885-7. doi: 10.1093/bioinformatics/btv290. Epub 2015 May 6"
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        version_file = os.path.join(self.directory,'bfc.version')
        if os.path.exists(version_file):
            with open(version_file,'r') as read:
                version = read.readlines()[0].rstrip()
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^bfc',line):
                        command_line = line
        return command_line

    def get_metadata(self):
        '''gets bfc stats. statsfile in this case is the readlength.sh results after bfc'''
        tmp = hasher()
        read_count_bfc = 0
        with open(self.statsfile, "r") as f:
            for line in f:
                if line.startswith("#Reads:"):
                    read_count_bfc = int(re.sub(r'^#Reads:\s+(\d+).*$',r'\1',line))
                                                                                                       

        #getting the input readcount from bfc logs
        read_count_input = 0
        logfile_bfc = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","*.bfc.e"))
        if len(logfile_bfc)==1:
            with open(logfile_bfc[0],"r") as f:
                for line in f.readlines():
                    if line.find("bfc_count_cb") !=-1 and line.find("processed") !=-1:
                        fields = line.split()
                        read_count_input += int(fields[3])
        tmp['label'] = "reads_filtered"
        tmp['file'] = 'bfc/input.corr.fastq.gz'
        tmp['metadata']['file_format']='fastq'
        tmp['metadata']['corrector']='bfc'
        tmp['metadata']['corrector_version']=self.version
        tmp['metadata']['corrector_parameters']=self.command_line
        tmp['metadata']['num_input_reads']=read_count_input
        tmp['metadata']['contam_filtered']=read_count_input - read_count_bfc
        tmp['metadata']['perc_artifact']=round(float(tmp['metadata']['contam_filtered'])/float(read_count_input)*100,1)
        return tmp

    def get_stats(self):
        '''bfc stats'''
        stats = ""
        input_reads= self.metadata['metadata']['num_input_reads']
        reads_removed = self.metadata['metadata']['contam_filtered']
        reads_remaining = input_reads - reads_removed
        pct_remaining = round(float(reads_remaining)/float(input_reads) * 100,1)
        stats += "The number of reads used as input to corrector is: " + str(input_reads) + "\n"
        stats += "The number of reads after correction is: " + str(reads_remaining) + " (" + str(pct_remaining) + "%)\n"
        return stats

class Analysis_bbcms(Analysis_tool):
    '''given an analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        self.readme_prefix = "Reads were corrected using bbcms"
        self.reference = "B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov"
        #self.version = self.get_version()
        #self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        return "Not used"
#        version = ""
#        version_file = os.path.join(self.directory,'bbcms.version')
#        if os.path.exists(version_file):
#            with open(version_file,'r') as read:
#                version = read.readlines()[0].rstrip()
#        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^bbcms',line):
                        command_line = line
        return command_line

    def get_metadata(self,debug=False):
        '''gets bbcms stats. statsfile in this case is the readlength.sh results after bfc'''
        tmp = hasher()
        counts_metadata = dict()
        logfile_bbcms = glob.glob(os.path.join(self.directory,"counts.metadata.json"))
        if(len(logfile_bbcms)==1):
            with open(logfile_bbcms[0])as f:counts_metadata=json.loads(f.read())
        example = {
            "Time": "Fri Feb 01 10:02:23 PST 2019",
            "Host": "cori06",
            "BBToolsVersion": "38.34",
            "JavaVersion": 1.8,
            "Command": "java -ea -Xmx60036m -Xms60036m -cp /bbmap/current/ bloom.BloomFilterCorrectorWrapper metadatafile=counts.metadata.json mincount=2 highcountfraction=0.6 in=/global/projectb/scratch/bfoster/metaG/debug/AUTOQC-2221/analysis.metag.spades.3.0.yaml.dir/head/out.fastq.gz out=/global/projectb/scratch/bfoster/metaG/debug/AUTOQC-2221/analysis.metag.spades.3.0.yaml.dir/bbcms/input.corr.fastq.gz",
            "Script": "bbcms.sh metadatafile=counts.metadata.json mincount=2 highcountfraction=0.6 in=/global/projectb/scratch/bfoster/metaG/debug/AUTOQC-2221/analysis.metag.spades.3.0.yaml.dir/head/out.fastq.gz out=/global/projectb/scratch/bfoster/metaG/debug/AUTOQC-2221/analysis.metag.spades.3.0.yaml.dir/bbcms/input.corr.fastq.gz",
            "ReadsIn": 1000000,
            "BasesIn": 142646268,
            "ReadsOut": 915222,
            "BasesOut": 133621198
            }
        if debug:
            print("Example: " + json.dumps(example,indent=3) + "\nActual:" + json.dumps(counts_metadata,indent=3))

        tmp['label'] = "reads_filtered"
        tmp['file'] = 'bbcms/input.corr.fastq.gz'
        tmp['metadata']['file_format']='fastq'
        tmp['metadata']['corrector']='bbcms'
        tmp['metadata']['corrector_version']=counts_metadata['BBToolsVersion']
        self.version = counts_metadata['BBToolsVersion']
        tmp['metadata']['corrector_parameters']=clean_command_line(counts_metadata['Script'])
        self.command_line = tmp['metadata']['corrector_parameters']
        tmp['metadata']['num_input_reads']=counts_metadata['ReadsIn']
        tmp['metadata']['contam_filtered']=counts_metadata['ReadsIn']-counts_metadata['ReadsOut']
        tmp['metadata']['perc_artifact']=round(float(tmp['metadata']['contam_filtered'])/float(tmp['metadata']['num_input_reads'])*100,1)
        return [tmp]

    def get_stats(self):
        '''bbcms stats'''
        stats = ""
        input_reads =  self.metadata[0]['metadata']['num_input_reads']
        reads_remaining =  self.metadata[0]['metadata']['num_input_reads'] - self.metadata[0]['metadata']['contam_filtered']
        percent_remaining = 100.0 - self.metadata[0]['metadata']['perc_artifact']
        stats += "The number of reads used as input to corrector is: " + str(input_reads) + "\n"
        stats += "The number of reads after correction is: " + str(reads_remaining) + " (" + str(percent_remaining) + "%)\n"
        return stats


class Analysis_tadpole(Analysis_tool):
    '''given an analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        self.readme_prefix = "Reads were corrected using tadpole and reads with no mate pair were removed"
        self.reference = "B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov"
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        version_file = glob.glob(os.path.join(self.directory,"*.tadpole.e"))[0]       
        if os.path.exists(version_file):
            with open(version_file,'r') as read:
                for line_ in read.readlines():
                    if line_.startswith("Version"):
                        version = line_.rstrip()
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^tadpole',line)or re.match(r'^shifter.*tadpole.*$',line):
                        command_line = line
        return command_line
#===============================================================================
# '''   {
#          "label": "reads_filtered",
#          "file": "bbcms/input.corr.fastq.gz",
#          "metadata": {
#             "file_format": "fastq",
#             "corrector": "bbcms",
#             "corrector_version": "38.34",
#             "corrector_parameters": "bbcms.sh metadatafile=counts.metadata.json mincount=2 highcountfraction=0.6 in=out.fastq.gz out=input.corr.fastq.gz",
#             "num_input_reads": 50000,
#             "contam_filtered": 9344,
#             "perc_artifact": 18.7
#          }
#       },'''
#===============================================================================

    def get_metadata(self):
        '''gets tadpole stats. statsfile in this case is the readlength.sh results after tadpole'''
        tmp = hasher()
        read_count_tadpole = 0
        with open(self.statsfile, "r") as f:
            for line in f:
                if line.startswith("#Reads:"):
                    read_count_tadpole = int(re.sub(r'^#Reads:\s+(\d+).*$',r'\1',line))
                                                                                                       

        #getting the input readcount from tadpole logs
        read_count_input = 0
        logfile_tadpole = glob.glob(os.path.join(self.directory,"*.tadpole.e"))
        if len(logfile_tadpole)==1:
            with open(logfile_tadpole[0],"r") as f:
                for line in f.readlines():
                    if line.startswith("Input"):
                        read_count_input = int(re.sub(r'^Input:\s+(\d+)\s+reads.*$',r'\1',line.rstrip()))
        tmp['label'] = "reads_filtered"
        tmp['file'] = 'tadpole/input.corr.fastq.gz'
        tmp['metadata']['file_format']='fastq'
        tmp['metadata']['corrector']='tadpole'
        tmp['metadata']['corrector_version']=self.version
        tmp['metadata']['corrector_parameters']=self.command_line
        tmp['metadata']['num_input_reads']=read_count_input
        tmp['metadata']['contam_filtered']=read_count_input - read_count_tadpole
        tmp['metadata']['perc_artifact']=round(float(tmp['metadata']['contam_filtered'])/float(read_count_input)*100,1)
        return tmp

    def get_stats(self):
        '''tadpole stats'''
        stats = ""
        input_reads= self.metadata['metadata']['num_input_reads']
        reads_removed = self.metadata['metadata']['contam_filtered']
        reads_remaining = input_reads - reads_removed
        pct_remaining = round(float(reads_remaining)/float(input_reads) * 100,1)
        stats += "The number of reads used as input to corrector is: " + str(input_reads) + "\n"
        stats += "The number of reads after correction is: " + str(reads_remaining) + " (" + str(pct_remaining) + "%)\n"
        return stats


class Analysis_dedupe(Analysis_tool):
    '''given an analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        self.readme_prefix = "Reads were deduplicated using clumpfy"
        self.reference = "B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov"
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        version_file = glob.glob(os.path.join(self.directory,"*.dedupe.e"))[0]       
        if os.path.exists(version_file):
            with open(version_file,'r') as read:
                for line_ in read.readlines():
                    if line_.startswith("Version"):
                        version = line_.rstrip()
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^clumpify',line) or re.match(r'^shifter.*clumpify.*$',line):
                        command_line = line
        return command_line

    def get_metadata(self):
        '''gets dedupe stats. statsfile in this case is the readlength.sh results after dedupe'''
        tmp = hasher()
        read_count_dedupe = 0
        with open(self.statsfile, "r") as f:
            for line in f:
                if line.startswith("#Reads:"):
                    read_count_dedupe = int(re.sub(r'^#Reads:\s+(\d+).*$',r'\1',line))
                                                                                                       

        #getting the input readcount from dedupe logs
        read_count_input = 0
        logfile_dedupe = glob.glob(os.path.join(self.directory,"*.dedupe.e"))
        if len(logfile_dedupe)==1:
            with open(logfile_dedupe[0],"r") as f:
                for line in f.readlines():
                    if line.startswith("Reads In:") and read_count_input == 0:
                        read_count_input = int(re.sub(r'^Reads In:.*?(\d+)$',r'\1',line.rstrip()))
        tmp['label'] = "reads_filtered"
        tmp['file'] = 'dedupe/input.corr.fastq.gz'
        tmp['metadata']['file_format']='fastq'
        tmp['metadata']['corrector']='clumpify'
        tmp['metadata']['corrector_version']=self.version
        tmp['metadata']['corrector_parameters']=self.command_line
        tmp['metadata']['num_input_reads']=read_count_input
        tmp['metadata']['contam_filtered']=read_count_input - read_count_dedupe
        tmp['metadata']['perc_artifact']=round(float(tmp['metadata']['contam_filtered'])/float(read_count_input)*100,1)
        return tmp

    def get_stats(self):
        '''tadpole stats'''
        stats = ""
        input_reads= self.metadata['metadata']['num_input_reads']
        reads_removed = self.metadata['metadata']['contam_filtered']
        reads_remaining = input_reads - reads_removed
        pct_remaining = round(float(reads_remaining)/float(input_reads) * 100,1)
        stats += "The number of reads used as input to deduplication is: " + str(input_reads) + "\n"
        stats += "The number of reads after deduplication is: " + str(reads_remaining) + " (" + str(pct_remaining) + "%)\n"
        return stats


class Analysis_bbmap(Analysis_tool):
    '''given a bbmap directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        logfile = glob.glob(os.path.join(self.directory,'command.bash.e'))
        if len(logfile)==1:
            self.logfile = logfile[0]
        self.readme_prefix = "The input read set was mapped to the final assembly and coverage information generated"
        self.reference = "B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov"
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                for line in read.readlines():
                    if re.match(r'^Version\s\d',line):
                        version = re.sub(r'^Version\s(\d\S+).*$',r'\1',line.rstrip())
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^bbmap.sh',line) or re.match(r'^shifter.*bbmap.sh.*$',line):
                        command_line = line
                        break
        return command_line

    def get_metadata(self):
        '''gets bbmap alignment file and coverage file '''
        output = list()

        tmp = hasher()
        read_count_bbmap_input = 0
        read_count_bbmap_mapped = 0
        with open(self.statsfile, "r") as f:
            for line in f:
                if line.startswith("Reads Used:"):
                    read_count_bbmap_input = int(re.sub(r'^Reads\sUsed:\s+(\d+).*$',r'\1',line))
                if line.startswith("Mapped reads:"):
                    read_count_bbmap_mapped = int(re.sub(r'^Mapped reads:\s+(\d+).*$',r'\1',line))
                                                                                                       
        if self.template == "metagenome":
            tmp['label'] = "metagenome_alignment"
            samfile = glob.glob(os.path.join(self.directory,"pairedMapped.sam.gz"))
            if len(samfile)==1:
                relative_path = os.path.join(os.path.basename(os.path.dirname(samfile[0])),os.path.basename(samfile[0]))
                tmp['file'] = relative_path
            tmp['metadata']['file_format']='sam'
            tmp['metadata']['aligner']=self.tool_name
            tmp['metadata']['aligner_version']=self.version
            tmp['metadata']['aligner_parameters']=self.command_line
            tmp['metadata']['num_input_reads']=read_count_bbmap_input
            tmp['metadata']['num_aligned_reads']=read_count_bbmap_mapped
            output.append(tmp)

            tmp = hasher()
            tmp["label"]= "metagenome_assembly_coverage"
            covfile = glob.glob(os.path.join(self.directory,"covstats.txt"))
            if len(covfile)==1:
                relative_path = os.path.join(os.path.basename(os.path.dirname(covfile[0])),os.path.basename(covfile[0]))
                tmp['metadata']['file_format']='cov'
                tmp["file"]= relative_path
            output.append(tmp)
        elif self.template =="metatranscriptome_assembly_and_alignment":
            tmp['label'] = "metatranscriptome_alignment"
            bam = glob.glob(os.path.join(self.directory,"*sorted.bam"))
            if len(bam)==1:
                rel_bam = os.path.join(os.path.basename(os.path.dirname(bam[0])),os.path.basename(bam[0]))
                tmp['file'] = rel_bam

            tmp['metadata']['file_format']='bam'
            tmp['metadata']['aligner']=self.tool_name
            tmp['metadata']['aligner_version']=self.version
            tmp['metadata']['aligner_parameters']=self.command_line
            tmp['metadata']['num_input_reads']=read_count_bbmap_input
            tmp['metadata']['num_aligned_reads']=read_count_bbmap_mapped
            output.append(tmp)
            
            tmp = hasher()
            tmp['metadata']['file_format']='bai'
            tmp['label'] = "metatranscriptome_alignment_index"
            idx = glob.glob(os.path.join(self.directory,"*sorted.bam.bai"))
            if len(idx) ==1 : 
                rel = os.path.join(os.path.basename(os.path.dirname(idx[0])),os.path.basename(idx[0]))
                tmp['file'] = rel
            output.append(tmp)

            tmp = hasher()
            tmp['metadata']['file_format']='cov'            
            tmp['label'] = "metatranscriptome_assembly_coverage"
            cov = glob.glob(os.path.join(self.directory,"covstats.txt"))
            if len(cov) ==1 : 
                rel = os.path.join(os.path.basename(os.path.dirname(cov[0])),os.path.basename(cov[0]))
                tmp['file'] = rel
            output.append(tmp)
                
        return output

    def get_stats(self):
        '''bbmap stats'''
        stats = ""
        input_reads= self.metadata[0]['metadata']['num_input_reads']
        reads_aligned = self.metadata[0]['metadata']['num_aligned_reads']
        pct_aligned = round(float(reads_aligned)/float(input_reads) * 100,1)
        covfile = glob.glob(os.path.join(self.directory,"covstats.txt"))
        m50 = 'NA' ; m90 = 'NA'
        if len(covfile)==1:
            (m50, m90) = get_m50_m90(covfile[0],input_reads)
        stats += "The number of reads used as input to aligner is: " + str(input_reads) + "\n"
        stats += "The number of reads aligned is: " + str(reads_aligned) + " (" + str(pct_aligned) + "%)\n"
        stats += "m50/m90 (length where 50% or 90% of reads align to contigs of this length or larger is: " + str(m50) + "/" + str(m90) + "\n"

        return stats

class Analysis_spades(Analysis_tool):
    '''given a spades directory.
       populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(Analysis_spades,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        logfile = glob.glob(os.path.join(self.directory,'*','spades.log'))
        if len(logfile)==1:
            self.logfile = logfile[0]

        self.readme_prefix = "The readset was assembled using SPAdes assembler"
        self.reference = "Bankevich et al. (2012) SPAdes: A New Genome Assembly Algorithm and Its Applications to Single-Cell Sequencing. J Comput. Biol., DOI: 10.1089/cmb.2012.0021."
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(Analysis_spades,self).get_readme() 
        
    def get_version(self):
        version = ""
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                for line in read.readlines():
                    if re.match(r'^\s+SPAdes version:',line):
                        version = re.sub(r'^\s+SPAdes version:\s*(\d\S+).*$',r'\1',line.rstrip())
        return version

    def get_command_line(self):
        command_line = ""
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                first_line = read.readlines()[0]
            if re.match(r'^Command line:\s',first_line):
                command_line = re.sub(r'^Command line:\s',r'',first_line)
        return command_line

    def get_metadata(self):
        '''gets spades '''
        outputs = list()

        #scaffolds_file
        tmp = hasher()
        bbstats = get_tsv(re.sub(r'txt$',r'tsv',self.statsfile))
        if self.template == "metagenome":
            tmp['label'] = "scaffolds"
            scaffolds_file = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","*.scaffolds.fasta"))
            relative_path = ""
            if len(scaffolds_file)==1:

                realpath = os.path.realpath(scaffolds_file[0])
                relative_path = os.path.join(os.path.basename(os.path.dirname(realpath)),os.path.basename(realpath))
            tmp['file'] = relative_path
        tmp['metadata']['file_format']='fasta'
        tmp['metadata']['assembler']=self.tool_name
        tmp['metadata']['assembler_version']=self.version
        tmp['metadata']['assembler_parameters']=self.command_line
        tmp['metadata'].update(bbstats)
        outputs.append(tmp)

        #contigs_file
        tmp = hasher()
        tmp['metadata'] = bbstats
        tmp['metadata'].update(bbstats)
        if self.template == "metagenome":
            tmp['label'] = "contigs"
            contigs_file = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","*.contigs.fasta"))
            if len(contigs_file)==1:
                realpath = os.path.realpath(contigs_file[0])
                relative_path = os.path.join(os.path.basename(os.path.dirname(realpath)),os.path.basename(realpath))
            tmp['file'] = relative_path
        tmp['metadata']['file_format']='fasta'
        tmp['metadata']['assembler']=self.tool_name
        tmp['metadata']['assembler_version']=self.version
        tmp['metadata']['assembler_parameters']=self.command_line
        outputs.append(tmp)
        return outputs

    def get_stats(self):
        '''spades stats'''
        with open(self.statsfile, "r") as f:
            stats = f.read()
        return stats

class Analysis_metaspades(Analysis_spades):
    '''given a metaspades directory.
       populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(Analysis_metaspades,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)        
        self.readme_prefix = "The readset was assembled using metaSPAdes assembler"
        self.reference = "metaSPAdes: a new versatile metagenomic assembler - Genome Res. 2017. 27:824-834.  doi: 10.1101/gr.213959.116"
        self.readme = super(Analysis_metaspades,self).get_readme()

 

class Analysis_megahit(Analysis_tool):
    '''given a megahit directory.
       populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(Analysis_megahit,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        logfile = glob.glob(os.path.join(self.directory,'*','log'))
        if len(logfile)==1:
            self.logfile = logfile[0]
        self.readme_prefix = "The readset was assembled using "
        self.reference = "MEGAHIT: An ultra-fast single-node solution for large and complex metagenomics assembly via succinct de Bruijn graph. Bioinformatics, 2015."
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(Analysis_megahit,self).get_readme() 

    def get_version(self):
        first_line = ""
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                first_line = read.readlines()[0].rstrip()
        return first_line

    def get_command_line(self):
        command_line = ""
        if os.path.exists(self.logfile):
            opts_file = os.path.join(os.path.dirname(self.logfile),"opts.txt")
            with open(opts_file,'r') as read:
                lines = read.readlines()
            lines = [x.rstrip() for x in lines if x.find("MEGAHIT_TEMP_DIR")==-1]
            command_line = "megahit " + " ".join(lines)
        return command_line

    def get_metadata(self):
        '''gets megahit metadata '''
        outputs = list()
        
        bbstats = get_tsv(re.sub(r'txt$',r'tsv',self.statsfile))

        #contigs_file
        tmp = hasher()
        tmp['metadata'] = bbstats
        tmp['metadata'].update(bbstats)
        if self.template == "metatranscriptome_assembly_and_alignment":
            tmp['label'] = "metatranscriptome_assembly"
            contigs_file = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","assembly.contigs.fasta"))
            if len(contigs_file)==1:
                realpath = os.path.realpath(contigs_file[0])
                relative_path = os.path.join(os.path.basename(os.path.dirname(realpath)),os.path.basename(realpath))
            tmp['file'] = relative_path
        tmp['metadata']['file_format']='fasta'
        tmp['metadata']['assembler']=self.tool_name
        tmp['metadata']['assembler_version']=self.version
        tmp['metadata']['assembler_parameters']=self.command_line
        outputs.append(tmp)
        return outputs

    def get_stats(self):
        '''stats'''
        with open(self.statsfile, "r") as f:
            stats = f.read()
        return stats

class Analysis_bbnorm(Analysis_tool):
    '''given a bbnorm analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory=directory, date=date, tool_name=tool_name, statsfile=statsfile, template=template)
        logfile = glob.glob(os.path.join(self.directory,'command.bash.e'))
        if len(logfile)==1:
            self.logfile = logfile[0]
        self.readme_prefix = "The reads were normalized using bbnorm"
        self.reference = "B. Bushnell: BBTools software package, http://bbtools.jgi.doe.gov"
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.metadata = self.get_metadata()
        self.stats = self.get_stats()
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        log = glob.glob(os.path.join(self.directory,'*bbnorm.e'))
        if len(log)==1:
            self.logfile = log[0]
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                for line in read.readlines():
                    if re.match(r'^.*bbmap\-\d',line):
                        version = re.sub(r'^.*bbmap\-(\d\S+?)\/.*$',r'\1',line.rstrip())
                        break
        if not version:
            version = "current version"
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^bbnorm.sh',line) or re.match(r'^shifter.*bbnorm.sh.*$',line):
                        command_line = line
                        break
        return command_line

    def get_metadata(self):
        '''bbnorm doesn't get imported'''
        '''statsfile should be the readlen.txt file on resulting readset'''
        tmp = hasher()
        read_count_input = 0
        logfile_bbnorm = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","*.bbnorm.e"))
        if len(logfile_bbnorm)==1:
            logfile_bbnorm = logfile_bbnorm[0]
        with open(logfile_bbnorm, "r") as f:
            for line in f:
                if line.startswith("Total reads in:"):
                    read_count_input = int(re.sub(r'^Total\sreads\sin:\s+(\d+).*$',r'\1',line))
                    break
                
        read_count_bbnorm = 0
        with open(self.statsfile, "r") as f:
            for line in f:
                if line.startswith("#Reads:"):
                    read_count_bbnorm = int(re.sub(r'^#Reads:\s+(\d+).*$',r'\1',line))
                    break
        
        tmp['label'] = "reads_normalized"
        tmp['file'] = 'bbnorm/input.norm.fastq.gz'
        tmp['metadata']['file_format']='fastq'
        tmp['metadata']['normalizer']='bbnorm'
        tmp['metadata']['normalizer_version']=self.version
        tmp['metadata']['normalizer_parameters']=self.command_line
        tmp['metadata']['num_input_reads']=read_count_input 
        tmp['metadata']['contam_filtered']=read_count_input - read_count_bbnorm
        tmp['metadata']['perc_artifact']=round(float(tmp['metadata']['contam_filtered'])/float(read_count_input)*100,1)
        return tmp

    def get_stats(self):
        '''tadpole stats'''
        stats = ""
        input_reads= self.metadata['metadata']['num_input_reads']
        reads_removed = self.metadata['metadata']['contam_filtered']
        reads_remaining = input_reads - reads_removed
        pct_remaining = round(float(reads_remaining)/float(input_reads) * 100,1)
        stats += "The number of reads used as input to normalization is: " + str(input_reads) + "\n"
        stats += "The number of reads after normalization is: " + str(reads_remaining) + " (" + str(pct_remaining) + "%)\n"
        return stats
    
def get_tsv(filename):
    '''takes 2 row tsv file and returns data structure'''
    tsv = dict()
    filename = glob.glob(filename)
    if len(filename)==1:
        stats = subprocess.Popen('cat ' + filename[0], shell=True, stdout=subprocess.PIPE).stdout.readlines()
        if len(stats)!=2:
            sys.exit("No valid stats file ")
        headers=re.sub(r'^#',r'',stats[0].decode('utf-8')).split()
        data=stats[1].split()
        data = [s.decode('utf-8') for s in data]
        if len(headers) != len(data):
            sys.exit("Not a valid stats file " + filename[0])
        cleaned_data = list()
        for i in data:
            if i.find('.') != -1:
                i = float(i)
            else:
                i = int(i)
            cleaned_data.append(i)
        tsv.update(dict(list(zip(headers,cleaned_data))))
    return tsv


def get_m50_m90(coverage_file,input_readcount):
    sum_val=0
    m50="NA"
    m90="NA"
    def isinteger(a):
        try:
            int(a)
            return True
        except ValueError:
            return False
        
    with open(coverage_file, "r") as f:
        for line in f.readlines():
            fields = line.rstrip().split()
            if not isinteger(fields[6]):
                continue
            sum_val+= int(fields[6]) + int(fields[7])
            if float(sum_val)/float(input_readcount) >0.5 and m50 == "NA":
                m50=fields[2]
            if float(sum_val)/float(input_readcount) >0.9 and m90 == "NA":
                m90=fields[2]
    return (m50,m90)
        
def clean_command_line(command):
    '''given a command line, reduce paths to basenames'''
    command_line = ""
    for argument in command.split():
        if argument.find('shifter')!=-1:
            argument = re.sub(r'shifter --',r'',argument)
        if argument.find('=')!=-1:
            (param,val) = argument.split('=')
            val=os.path.basename(val)
            argument = param + "=" + val
        else:
            argument = os.path.basename(argument)
        command_line += argument + " "
    return command_line.rstrip()
    
def hasher():
    return collections.defaultdict(hasher)

if __name__ == '__main__':
    main()

#=================================END==============================================
class Analysis_(Analysis_tool):
    '''Generic template for creating Analysis_tool subclasses'''
    '''given an analysis directory, populate version, command line , reference, readme, stats'''
    def __init__(self,directory="",date="", tool_name="", statsfile="", template=""):
        super(self.__class__,self).__init__(directory, date, tool_name, statsfile)
        logfile = glob.glob(os.path.join(self.directory,'command.bash.e'))
        if len(logfile)==1:
            self.logfile = logfile[0]
        self.readme_prefix = ""
        self.reference = ""
        self.version = self.get_version()
        self.command_line = clean_command_line(self.get_command_line())
        self.readme = super(self.__class__,self).get_readme()

    def get_version(self):
        version = ""
        if os.path.exists(self.logfile):
            with open(self.logfile,'r') as read:
                for line in read.readlines():
                    line.find("")
                    break
        return version 

    def get_command_line(self):
        command_line = ""
        command_file = glob.glob(os.path.join(self.directory,'command.bash'))
        if len(command_file)==1:
            command_file = command_file[0]
        else:
            command_file = "Not Found"
        if os.path.exists(command_file):
            with open(command_file,'r') as read:
                for line in (read.readlines()):
                    if re.match(r'^bbmap.sh',line) or re.match(r'^shifter.*bbmap.sh.*$',line):
                        command_line = line
                        break
        return command_line
# ==========================================================================================================================================                    
'''
    {
         "label": "reads_filtered",
         "file": "bfc/input.corr.fastq.gz",
         "metadata": {
            "perc_artifact": 50.13,
            "num_input_reads": 133012118,
            "file_format": "fastq",
            "corrector": "bfc",
            "corrector_version": "r181",
            "corrector_parameters": " bfc -1 -s 10g -k 21 -t 10 out.fastq.gz",
            "contam_filtered": 66682628
         }
      },
'''

'''
     {
         "label": "metagenome_alignment",
         "file": "readMappingPairs/pairedMapped.sam.gz",
         "metadata": {
            "file_format": "bam",
            "aligner_parameters": "bbmap.sh nodisk=true interleaved=true ambiguous=random in=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/head/out.fastq.gz ref=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/createAGPfile/assembly.contigs.fasta out=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/pairedMapped.bam covstats=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/covstats.txt bamscript=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/to_bam.sh && /global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/to_bam.sh && reformat.sh in=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/pairedMapped.bam out=/global/projectb/sandbox/gaag/bfoster/metaG/AUTOQC-1390/BZZHX/readMappingPairs/pairedMapped.sam.gz",
            "aligner": "bbmap",
            "aligner_version": "37.78",
            "num_input_reads": 133012118.0,
            "num_aligned_reads": 60121098.0
         }
      },

'''
'''
   {
         "label": "scaffolds",
         "file": "createAGPfile/final.scaffolds.fasta",
         "metadata": {
            "assembler_version": "3.11.1",
            "assembler": "spades",
            "assembler_parameters": "spades.py -m 2000 --tmp-dir $SLURM_TMP -o spades3 --only-assembler -k 33,55,77,99,127 --meta -t 32  ",
...
            "scaf_n_gt50K": 34.0,
            "ctg_L50": 420.0,
            "ctg_L90": 284.0,
            "scaf_L50": 421.0,
            "scaf_bp": 800784863.0
         }
      },

'''                
'''
  {
         "label": "reads_filtered",
         "file": "bfc/input.corr.fastq.gz",
         "metadata": {
            "perc_artifact": 50.13,
            "num_input_reads": 133012118,
            "file_format": "fastq",
            "corrector": "bfc",
            "corrector_version": "r181",
            "corrector_parameters": " bfc -1 -s 10g -k 21 -t 10 out.fastq.gz",
            "contam_filtered": 66682628
         }
      },
'''

# def get_stats(self):
#         '''gets bbnorm stats. statsfile in this case is the readlength.sh results after bbnorm'''
#         stats = ""
#         read_count_bbnorm = 0
#         with open(self.statsfile, "r") as f:
#             for line in f:
#                 if line.startswith("#Reads:"):
#                     read_count_bbnorm = int(re.sub(r'^#Reads:\s+(\d+).*$',r'\1',line))
                                                                                                       

#         #getting the input readcount from bnorm logs
#         read_count_input = 0
#         logfile_bbnorm = glob.glob(os.path.join(os.path.dirname(self.statsfile),"..","*","*.bbnorm.e"))
#         if len(logfile_bbnorm)==1:
#             logfile_bbnorm = logfile_bbnorm[0]
#             with open(logfile_bbnorm, "r") as f:
#                 for line in f.readlines():
#                     if line.startswith("Total reads in"):
#                         read_count_input = int(re.sub(r'^Total\sreads\sin:\s+(\d+).*$',r'\1',line))
#                         break
#         else:
#             sys.stderr.write("no or multiple bbnorm.e log files found")
                        

#         return stats

