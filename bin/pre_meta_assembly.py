#!/usr/bin/env python

'''This is a setup which provides basic metadata and routing, template mappings
    input: file(s), template.yaml, atid
    requires: webservice access to metadata from: pmo, sdm, rqc
    output:json file with basic metadata and pipeline/template information
'''

'''
    todo:
    1) move to jgi-meta repo
    1) double check multi metadata, rqc-stats readME etc ... in.progress
    
    3) krona to post process
    4) setup/reporting for mto projects

    todo later:
    1) nose tests unit test for routines in module
    2) wdl templates
    2) later: advanced routing predict final kmer cound based on median/ave depth, uniqueue kmer count
    3) reporting pipeline with outputs.metadata.json for krona, pdf , sigs, finalize (redo reporting pipelines to expand out individual steps)
    4) prepackage mode with tar file
'''
import sys, os
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import report_utils as rep
import argparse
import json

VERSION = "1.0.0"
def main():
    '''Outputs json file with jgi specific project metadata for analysis and reporting'''
    parser = argparse.ArgumentParser(description=main.__doc__)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('files', nargs="*",default=[], help="files to operate on")
    group.add_argument('--atid',required=False, help="atid to gather")
    group.add_argument('--apid',required=False, help="apid to gather")

    parser.add_argument("-o", "--outputfile", required=False, help="output file json. default stdout\n")
    parser.add_argument("-u", "--include_unusable", required=False, default=False, action='store_true', help="allow filtered_files where usable=False.\n")
    parser.add_argument("-i", "--include_closed", required=False, default=False, action='store_true', help="include closed at from consideration default = True.\n")
    parser.add_argument("-f", "--force_latest", required=False, default=False, action='store_true', help="use the latest analysis task.\n")
    parser.add_argument("-t", "--templates", required=False, default=False, action='store_true', help="add template files run.yaml to same dir as outputfile.\n")
    
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-d", "--debug", required=False, default = False, action='store_true', help="debugging.\n")
    parser.add_argument("-y", "--toy", required=False, default = False, action='store_true', help="use toy dataset.\n")
    args = parser.parse_args()

    if args.outputfile and os.path.isabs(args.outputfile):
        outputdir = os.path.dirname(args.outputfile())
    elif args.outputfile:
        outputdir = "./"
    else:
        outputdir = ""#will print to stdout
    
    apid = int()
    if args.atid:
        args.apid=rep.atid_to_apid(args.atid)
    if args.apid:
        apid=args.apid
        ap = rep.web_services('pmo', 'ap', apid)['uss_analysis_project']
        apid_product=ap['analysis_product_id']
        spids = [spid['sequencing_project_id']  for spid in ap['sequencing_projects']]
    else:#expand input files to include other filtered files (usable or not) from input files spid list
        sequnits = [rep.filename_to_sequnit(filename) for filename in args.files]
        spids = list(set([rep.sequnit_to_lib_spid(sequnit)['sequencing_project_id'] for sequnit in sequnits]))
        apids = list()
        for spid in spids:
            apids += rep.web_services('pmo', 'spid', spid)
            
        if len(apids)>1:
            tmp_apids = list()
            for ap in apids:
                ap_keep = False
                for task in ap['uss_analysis_project']['analysis_tasks']:
                    if task['analysis_task_type_id'] in [47,83]:
                        ap_keep=True
                if ap_keep:
                    tmp_apids.append(ap)
            apids = tmp_apids
            if len(apids)>1:
                print("More than one valid apid per files " + json.dumps(apids,indent=3))
                sys.exit()
            apid=apids[0]['uss_analysis_project']['analysis_project_id']
            apid_product=apids[0]['uss_analysis_project']['analysis_product_id']
        else:
            apid=apids[0]['uss_analysis_project']['analysis_project_id']
            apid_product=apids[0]['uss_analysis_project']['analysis_product_id']
 
    '''"58_47":{"name":"Metagenome Annotated Minimal Draft","fd":[12],"analysis_template":2},
        "7_47":{"name":"Metagenome Annotated Standard Draft","fd":[13],"analysis_template":2},
        "84_47":{"name":"Metagenome Viral Annotated Standard Draft","fd":[87],"analysis_template":2},
        "10_83":{"name":"Metagenome Annotated Metatranscriptome","fd":[15],"analysis_template":3},
        "38_83":{"name":"Metagenome Annotated Metatranscriptome (combined)","fd":[15,71],"analysis_template":3},
        "68_83":{"name":"Co-Assembly - Metagenome Metatranscriptome","fd":[71],"analysis_template":3},
        "61_83":{"name":"Eukaryote Community Metatranscriptome","fd":[65],"analysis_template":3},
        "69_83":{"name":"Co-Assembly - Eukaryotic Community Metatranscriptome","fd":[72],"analysis_template":3},
        "101_47":{"name":"Metagenome Viral Annotated Minimal Draft","fd":[95],"analysis_template":4},
        "53_47":{"name":"Cell Enrichment unamplified Random","fd":[59],"analysis_template":4},
        "55_47":{"name":"Cell Enrichment unamplified Targeted","fd":[61],"analysis_template":4}
    '''
    at_type = ""
    if apid_product in [58, 7, 84]:#metag
        at_type = 47
    elif apid_product in [10, 38, 68, 61, 69]:#metat
        at_type = 83
    elif apid_product in [101, 53, 55]:#lowv
        at_type = 47
    else:
        sys.exit("Assy type not recognized:" + str(apid_product))
    
    tasks = rep.task_getter(apid=apid,task_type=at_type,exclude_closed=not(args.include_closed), force_latest=args.force_latest)
    if len(tasks)==1:
        task = tasks[0]
    else:
        sys.exit("Multitask")
    
    purged = False
    filtered_files = list()
    sequnits = list()
    libraries = list()
    for spid in task['spid']:
        task['spid'][spid]=rep.spid_to_filtered_info(spid)
        for filt in task['spid'][spid]:
            if filt["file_status_id"] in [10]:
                print('jamo fetch all id ' + filt['_id'])
                purged=True
            elif filt['usable'] !=0 or args.include_unusable:
                filtered_files.append(filt['full_path'])
                sequnits.append(filt['seq_unit_name'])
                libraries.append(filt['library_name'])
    if purged:
        sys.exit("restore your sequnits")
        
    atid = task['at']['analysis_task_id']
    output=rep.hasher()
    output['inputs']=filtered_files
    output['metadata']['seq_unit_name']=sequnits
    output['metadata']['library_name']=list(set(libraries))
    output['metadata']['sequencing_project_id']=list(set(spids))
    output['metadata']['analysis_project_id']=[apid]
    output['metadata']['analysis_task_id']=[atid]
    output['metadata']['pipeline_metadata']=task
    ap_at = str(task['ap']['analysis_product_id']) + "_" + str(task['at']['analysis_task_type_id'])
    output['metadata']['pipeline_metadata']['execution_metadata']=rep.rqc_pipeline_queue_and_templates_by_ap_at(ap_at,sequnits)   #sequnits

    if args.debug:
        print(json.dumps(output,indent=3))

    if args.templates and outputdir:
        outputdir = os.path.realpath(os.path.dirname(args.outputfile))
        templates=list()
        template_data = {"_inputs_":" ".join(output['inputs'])}# _inputs_ is analysis template
        template_data.update({"_inputdir_":outputdir}) #, _inputdir_ is reporting
        template_data.update({"_bin_dir_":os.path.dirname(__file__)})
        template_data.update({"_bin_dir_":os.path.dirname(__file__)})
        template_data.update({"_conda_dir_":"/global/projectb/sandbox/gaag/bfoster/miniconda3/bin"})
        if args.toy:
            template_data['_inputs_'] = rep.get_glob(os.path.join(template_data['_bin_dir_'],"..","test_data","*.fastq.gz"),latest=True)
                                                                  
        for template_type in ['analysis','report']:
            template = output['metadata']['pipeline_metadata']['execution_metadata']['templates'][template_type]
            template_in = os.path.join(os.path.dirname(__file__),"..","templates",template)
            template_out = os.path.join(outputdir,"run." + template_type + ".yaml")
            templates.append(rep.template_render(template_in, template_data, template_out))
        to_run = os.path.join(outputdir,"run.bash")
        log = os.path.join(outputdir,"status.log")
        pr = os.path.join(os.path.dirname(__file__),"pipeline_runner.py")
        with open(to_run,"w") as f:
            f.write('#!/usr/bin/env bash' + "\n\n")
            f.write('#SBATCH -t 100:00:00 --mem=480G -D ' + outputdir + "\n\n")
            f.write('source /global/projectb/sandbox/gaag/bfoster/miniconda3/bin/activate' + "\n")
            f.write('export PATH=/global/projectb/sandbox/gaag/bfoster/miniconda3/bin:$PATH' + "\n")
            f.write("ulimit -c 0\numask 002\nset -eo pipefail\n\n")
            for temp in templates:
                f.write("cd " + outputdir + "\n\n" + pr + " -f " + temp + " >> " + log + "\n\n")
        with open(args.outputfile,"w") as f:f.write(json.dumps(output,indent=3))
    else:
        sys.stdout.write(json.dumps(output,indent=3))
    return 

if __name__ == '__main__':
    main()

