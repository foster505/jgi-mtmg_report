#!/usr/bin/env python

import sys, os
import argparse

sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import report_utils as rep
import json

VERSION = "1.0.0"
def main():
    '''This program takes an AP or AT and returns mapping to other data including:
    -reads file(s)
    -ref files(s)
    -pipeline_template
    '''
    '''
    2) crate AT structure with
   FD/AP/AT
   sourceAP
   actual_sourceAP
   ref file with date and status
   reads file(s) with date and status
   #later
   templates section:
        inputs: reference, reads
'''
    parser = argparse.ArgumentParser(description=main.__doc__)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--atid',required=False, help="atid to gather")
    group.add_argument('--apid',required=False, help="apid to gather")
    
    parser.add_argument("--outputfile", required=False, help="output file json. default stdout\n")
    parser.add_argument("--templates", required=False, default=False, action='store_true', help="add template files run.yaml to same dir as outputfile.\n")        
    parser.add_argument("--legacy", required=False, default=False, action='store_true', help="create legacy command file.\n")        
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-d", "--debug", required=False, default = False, action='store_true', help="debugging.\n")
    args = parser.parse_args()

    if args.apid:
        apid=args.apid
    elif args.atid:
        apid = rep.atid_to_apid(args.atid)
        
    tasks = rep.task_getter(apid=apid,task_type='Mapping to Reference')
    
    
    #flesh out tasks
    if len(tasks)==1:
        task = tasks[0]
    else:
        sys.exit("Multitask")
    
    purged = False    
    for spid in task['spid']:
        task['spid'][spid]=rep.spid_to_filtered_info(spid)
        for filt in task['spid'][spid]:
            if filt["file_status_id"] in [10]:
                print('jamo fetch all id ' + filt['_id'])
    if purged:
        sys.exit("restore your sequnits")
        
    task['actual_source_ap'] = rep.ap_to_assembly_contigs(task['actual_source_ap'])
    if task['actual_source_ap']["file_status_id"] in [10]:
        print('jamo fetch all id ' + task['actual_source_ap']['_id'])
    
    #mkdir 1198480;cd 1198480;/global/projectb/sandbox/gaag/bfoster/jgi-mt/bin/map_mt_reads.py /global/dna/dm_archive/gaag/analyses/AUTO-194026/final.contigs.fasta /global/dna/dm_archive/rqc/analyses/AUTO-144822/12184.1.243016.CGTACG.filter-MTF.fastq.gz --local --cov --norun
    if args.legacy and len(task['spid'].keys())==1:
        spid= task['spid'][list(task['spid'].keys())[0]]
        if len(spid)==1:
            fp = spid[0]['full_path']
            cmd = "mkdir " + str(apid) + ";cd " + str(apid) 
            cmd += "; /global/projectb/sandbox/gaag/bfoster/jgi-mt/bin/map_mt_reads.py  --local --cov --norun "
            cmd += " " + task['actual_source_ap']['full_path']
            cmd += " " + fp
            cmd += ";cd .. "
            task['cmd']=cmd
        
    if args.outputfile:
        with open (args.outputfile,"w") as f:
            f.write(json.dumps(task,indent=3))
    else:
        sys.stdout.write(json.dumps(task,indent=3))

    return

    
if __name__ == '__main__':
    main()
