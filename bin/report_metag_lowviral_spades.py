#!/usr/bin/env python

import sys, os
import json
import argparse
from posix import mkdir

sys.path.insert(0, os.path.join(os.path.dirname(__file__),"..",'lib'))
import report_utils as rep
import tool_info_utils as readme_tool
VERSION = "1.0.0"

def main():
    '''Creates readme reports, graphical reports and expanded metadata.json file for downstream processing.
    input: setup.json file basic metadata.json stub
    output: README files, modified metadata.json file'''
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-i", "--inputdir", required=True, help="Analysis dir.\n")
    parser.add_argument("-o", "--outputdir", required=True, help="output dir.\n")
    parser.add_argument("-j", "--json", required=True, help="setup json file.\n")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-d", "--debug", required=False, default=False,action='store_true', help="debug.\n")
    args = parser.parse_args()

    if not(os.path.exists(args.outputdir)):
        mkdir(args.outputdir)
        
    if not os.path.exists(args.json):
        sys.exit("Can't find " + args.json)

    with open(args.json, "r") as f:
        meta = json.loads(f.read())

    #gather files
    paths = dict()
    #paths for stats
    paths['tadpole_dir']=rep.get_glob(os.path.join(args.inputdir,"tadpole"))
    paths['tadpole_stats']=rep.get_glob(os.path.join(args.inputdir,"readstats_tadpole","readlen.txt"))
    paths['dedupe_dir']=rep.get_glob(os.path.join(args.inputdir,"dedupe"))
    paths['dedupe_stats']=rep.get_glob(os.path.join(args.inputdir,"readstats_dedupe","readlen.txt"))
    paths['assembly_dir']=rep.get_glob(os.path.join(args.inputdir,"assy"))
    paths['assembly_stats']=rep.get_glob(os.path.join(args.inputdir,"assembly_stats","*.scaffolds.fasta.stats.txt"))
    paths['alignment_dir']=rep.get_glob(os.path.join(args.inputdir,"read_mapping_pairs"))
    paths['alignment_stats']=rep.get_glob(os.path.join(args.inputdir,"read_mapping_pairs","command.bash.e"))

    #paths for files
    paths['covfile']=rep.get_glob(os.path.join(args.inputdir,'read_mapping_pairs','covstats.txt'))

    project_info = rep.metadata_record_to_report_header(meta) 
    report_text = project_info['report_header']
    meta['metadata']['subtitle']=project_info['subtitle']
    report_text += "Input Data:\n"

    input_info=list()
    #gather all input sequence stats into a list
    for inputfile in meta['inputs']:
        input_info.append(rep.filtered_file_to_report_text(inputfile))
    meta['metadata']['seq_info'] = input_info
    
    for info in input_info:
        if info['readme_text']:
            report_text += "\t" + "\n\t".join(info['readme_text'].rstrip().split("\n")) + "\n\n"
        else:
            report_text += "\t" + inputfile + "\n\n"

    if len(input_info)>1:
        report_text += "Summary Read Pre-processing:\n"
        report_text += "\tTotal Filtered Read Count: " + str(sum([info['info']['filtered_count'] for info in input_info])) + "\n"
        report_text += "\tTotal Filtered Base Count: " + str(sum([info['info']['filtered_base_count'] for info in input_info])) + "\n"
        report_text += "\tTotal Raw Read Count: " + str(sum([info['info']['raw_count'] for info in input_info])) + "\n"
        report_text += "\tTotal Raw Base Count: " + str(sum([info['info']['raw_base_count'] for info in input_info])) + "\n\n"


    tadpole_metadata = readme_tool.Analysis_tadpole(tool_name='tadpole',directory=paths['tadpole_dir'], statsfile=paths['tadpole_stats'], template="")
    report_text += "Read Correction Stats:\n" + "\t" + "\n\t".join(tadpole_metadata.dump()['stats'].rstrip().split("\n")) + "\n\n";        

    dedupe_metadata = readme_tool.Analysis_dedupe(tool_name='dedupe',directory=paths['dedupe_dir'], statsfile=paths['dedupe_stats'],template='')
    report_text += "De-duplication Stats:\n" + "\t" + "\n\t".join(dedupe_metadata.dump()['stats'].rstrip().split("\n")) + "\n\n";        
 
    assembly = readme_tool.Analysis_metaspades(tool_name='metaspades',directory=paths['assembly_dir'], statsfile=paths['assembly_stats'], template="metagenome")
    report_text += "Assembly Stats:\n" + "\t" + "\n\t".join(assembly.dump()['stats'].rstrip().split("\n")) + "\n\n";

    alignment =readme_tool.Analysis_bbmap(tool_name='bbmap',directory=paths['alignment_dir'], statsfile=paths['alignment_stats'], template='metagenome') 
    report_text += "Alignment Stats:\n" + "\t" + "\n\t".join(alignment.dump()['stats'].rstrip().split("\n")) + "\n\n";

    report_text += "Methods:\n"
    tadpole_text =  rep.methods_format_wrap(tadpole_metadata.dump()['readme']) + "\n"
    report_text += tadpole_text.replace("__REF__","1")
    dedupe_text =  rep.methods_format_wrap(dedupe_metadata.dump()['readme']) + "\n"
    report_text += dedupe_text.replace("__REF__","1")
    assy_text = rep.methods_format_wrap(assembly.dump()['readme']) + "\n"
    report_text += assy_text.replace("__REF__","2")
    aln_text = rep.methods_format_wrap(alignment.dump()['readme']) + "\n"
    report_text += aln_text.replace("__REF__","1")
    
    report_text += "(1) " + rep.methods_format_wrap(tadpole_metadata.dump()['reference'])
    report_text += "(2) " + rep.methods_format_wrap(assembly.dump()['reference'])

    report_text += "\nIf you have any questions, please contact the JGI project manager.\n"
    if paths['covfile']:
        (cov, gccov, gc) = rep.generate_graphics(paths['covfile'],args.outputdir)
    
    (txtout, htmlout, pdfout) = rep.generate_pdf(txt=report_text, graphics=[cov, gccov, gc], outdir=os.path.join(args.inputdir, "report"), prefix='README')
    paths['readme']=txtout
    paths['readme_pdf']=pdfout
    paths['readme_html']=htmlout
    
    outputs=list()
    outputs.append(tadpole_metadata.get_metadata())
    outputs+=assembly.get_metadata()
    outputs+=alignment.get_metadata()
    outputs.append({"label":"text_report","file":"report/README.txt","metadata":{"file_format":"text"}})
    outputs.append({"label":"graphical_report","file":"report/README.pdf","metadata":{"file_format":"pdf"}})

    meta['outputs']=outputs

    with open(os.path.join(args.outputdir,"metadata.json"), "w") as f: f.write(json.dumps(meta,indent=3))
    return
                        #### END FUNCTIONS ####
if __name__ == "__main__":
    main()
